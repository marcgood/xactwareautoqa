package com.xactware;

import java.lang.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.*;

public class testOne {
    // Instantiate
    WebDriver driver;
    WebDriverWait wait;
    Actions actions;
    WebElement mainMenu;
    WebElement textLink;
    JavascriptExecutor js;
    String xaHeaderText;
    String baseURL;


    @BeforeSuite
    public void SetUp() {
        driver = new ChromeDriver();
        baseURL = "https://www.xactware.com";
        wait = new WebDriverWait(driver, 5);
    }

    @AfterSuite
    public void TearDown() {
        driver.quit();
    }

    @BeforeTest
    public void OpenURL() {
        driver.navigate().to(baseURL);
    }

    @AfterTest
    public void CloseDriver() {
        driver.close();
    }

    @Test
    public void ShouldLoadAnalysisPage_WhenSelectedFromProductList() {
        String urlTitle = "Claims Management Software | XactAnalysis";
        // Select Products, then submenu Xactanalysis
        selectSubMenu("Products", "XactAnalysis");

        //Assert: Check its title is correct
        Assert.assertEquals(urlTitle, driver.getTitle(), "Title check failed!");
    } // end ShouldLoadAnalysisPage_WhenSelectedFromProductList Test

    @Test
    public void ShouldOpenSubPage_WhenLeftNavigationIsClicked() {
        // Select Products, then submenu Xactanalysis
        selectSubMenu("Products", "XactAnalysis");

        // Features
        xaValidateSubPages("Features", "XactAnalysis Features");
        // Mobile
        xaValidateSubPages("Mobile", "Access XactAnalysis Anywhere");
        // XactAnalysis Network
        xaValidateSubPages("XactAnalysis Network", "What is the XactAnalysis Network?");
        // Quality Review (QR)
        // This fails due to defect in header
        //xaValidateSubPages("Quality Review (QR)", "XactAnalysis Quality Review (QR)");
        // Management Dashboards
        xaValidateSubPages("Management Dashboards", "Smart reports. Smarter insights.");
        // ... and so forth
    }

    private void selectSubMenu(String mMenu, String sMenu) {
        actions = new Actions(driver);
        mainMenu = driver.findElement(By.linkText(mMenu));
        actions.moveToElement(mainMenu).perform();
        driver.findElement(By.linkText(sMenu)).click();
        // Wait for Xactanalysis page to load
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_Breadcrumbs")));
    }

    private void xaValidateSubPages(String xaSubPage, String xaPageHeader) {
        // Scroll until Link is Present
        js = (JavascriptExecutor) driver;
        textLink = driver.findElement(By.linkText(xaSubPage));
        js.executeScript("arguments[0].scrollIntoView();", textLink);
        // Click the xaLink
        textLink.click();
        // Wait until new page loads
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ctl00_Breadcrumbs"), xaSubPage));
        // Get text from header of page
        xaHeaderText = driver.findElement(By.xpath("//*[@id=\"main\"]/div[2]/div/h1")).getText();

        Assert.assertEquals(xaHeaderText, xaHeaderText, "The " + xaSubPage + " page did not load.");

    }

}
